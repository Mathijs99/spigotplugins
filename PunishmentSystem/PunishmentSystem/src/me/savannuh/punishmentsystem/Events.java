package me.savannuh.punishmentsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

public class Events implements Listener {

	FileConfiguration config;
	Main main;
	
	String prefix = ChatColor.AQUA + "<<" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "PunishmentSystem"
			+ ChatColor.AQUA + ">> ";

	public Events(Main main) {

		this.main = main;
		config = main.getConfig();

	}

	@EventHandler
	public void onLogin(PlayerLoginEvent event) {

		if (config.getConfigurationSection("verbannen").getKeys(false)
				.contains(event.getPlayer().getUniqueId().toString())) {

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Calendar calendar = Calendar.getInstance();

			if (dateFormat.format(calendar.getTime())
					.compareTo(config.getString("verbannen." + event.getPlayer().getUniqueId() + ".unban")) >= 0) {

				config.set("verbannen." + event.getPlayer().getUniqueId(), null);

				main.saveConfig();

				return;

			}

			String message = "";

			if (config.getString("verbannen." + event.getPlayer().getUniqueId() + ".unban").equals("permanent")) {
				
				message = ChatColor.DARK_RED + "" + ChatColor.BOLD + "PERMANENT VERBANNEN\n\n" + ChatColor.WHITE + "Reden: "
						+ config.getString("verbannen." + event.getPlayer().getUniqueId() + ".reden");

			} else {

				message = ChatColor.RED + "" + ChatColor.BOLD + "TIJDELIJK VERBANNEN\n\n" + ChatColor.WHITE + "Reden: "
						+ config.getString("verbannen." + event.getPlayer().getUniqueId() + ".reden") + "\nTot: "
						+ config.getString("verbannen." + event.getPlayer().getUniqueId() + ".unban");

			}

			event.disallow(Result.KICK_BANNED, message);

			return;

		}

	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {

		if (config.getConfigurationSection("muted").getKeys(false)
				.contains(event.getPlayer().getUniqueId().toString())) {

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Calendar calendar = Calendar.getInstance();

			if (dateFormat.format(calendar.getTime())
					.compareTo(config.getString("muted." + event.getPlayer().getUniqueId() + ".unmute")) >= 0) {

				config.set("muted." + event.getPlayer().getUniqueId(), null);

				main.saveConfig();

				return;

			}

			event.setCancelled(true);

			event.getPlayer().sendMessage(prefix + ChatColor.RED + "U kunt niet meer typen tot "
					+ main.getConfig().getString("muted." + event.getPlayer().getUniqueId() + ".unmute")
					+ " met als reden: \""
					+ main.getConfig().getString("muted." + event.getPlayer().getUniqueId() + ".reden") + "\".");

			return;

		}

	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		
		if (config.getStringList("permissions.staffleden").contains(event.getPlayer().getUniqueId().toString())) {
			
			Permission.setUp(event.getPlayer(), main);

		}
			
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		
		if (main.staffList.contains(event.getPlayer().getName())) {
			
			event.getPlayer().removeAttachment(Permission.getAttachment());
			
		}
		
	}

}
