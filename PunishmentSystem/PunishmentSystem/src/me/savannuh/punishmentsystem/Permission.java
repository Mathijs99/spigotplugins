package me.savannuh.punishmentsystem;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

public class Permission {
	
	private static PermissionAttachment attachment;

	public static void setUp(Player player, Main main) {

		if (main.getConfig().getStringList("permissions.staffleden").contains(player.getUniqueId().toString())) {
			
			attachment = player.addAttachment(main);
			attachment.setPermission("perm.staff", true);
		}

	}
	
	public static PermissionAttachment getAttachment() {
		
		return attachment;
		
	}

}
