package me.savannuh.punishmentsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	ArrayList<String> staffList = new ArrayList<String>();

	String prefix = ChatColor.AQUA + "<<" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "PunishmentSystem"
			+ ChatColor.AQUA + ">> ";

	public void onEnable() {

		Bukkit.getServer().getPluginManager().registerEvents(new Events(this), this);

		this.getConfig().options().copyDefaults(true);

		for (String uuid : this.getConfig().getStringList("permissions.staffleden")) {

			staffList.add(uuid);

		}

		this.saveConfig();

	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("staff") || cmd.getName().equalsIgnoreCase("unstaff")) {

			if (sender instanceof Player) {

				if (!sender.isOp()) {

					return true;

				}

			}

			if (cmd.getName().equalsIgnoreCase("staff")) {

				if (args.length != 1) {

					sender.sendMessage(ChatColor.YELLOW + "/staff <spelersnaam>");

					return true;

				}

				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);

				if (staffList.contains(offlinePlayer.getUniqueId().toString())) {

					sender.sendMessage(ChatColor.RED + "De speler " + args[0] + " heeft al staff permissies.");

					return true;

				}

				staffList.add(offlinePlayer.getUniqueId().toString());

				this.getConfig().set("permissions.staffleden", staffList);

				this.saveConfig();

				sender.sendMessage(
						ChatColor.GREEN + "De speler " + args[0] + " heeft succesvol staff permissies ontvangen.");

				if (Bukkit.getPlayer(args[0]) != null) {

					Bukkit.getPlayer(args[0]).sendMessage(
							ChatColor.GREEN + "U heeft uw staff permissies ontvangen. Rejoin om ze te ontvangen.");

				}

				return true;

			}

			if (args.length != 1) {

				sender.sendMessage(ChatColor.YELLOW + "/unstaff <spelersnaam>");

				return true;

			}

			OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);

			if (!staffList.contains(offlinePlayer.getUniqueId().toString())) {

				sender.sendMessage(ChatColor.RED + "De speler " + args[0] + " had al geen staff permissies.");

				return true;

			}

			staffList.remove(offlinePlayer.getUniqueId().toString());

			this.getConfig().set("permissions.staffleden", staffList);

			this.saveConfig();

			sender.sendMessage(ChatColor.GREEN + "De speler " + args[0] + " heeft nu geen staff permissies meer.");

			return true;

		}

		if (sender instanceof Player) {

			if (!sender.hasPermission("perm.staff")) {

				return true;
			}
		}

		if (cmd.getName().equalsIgnoreCase("ban")) {

			if (args.length < 3) {

				sender.sendMessage(prefix + ChatColor.YELLOW + "/ban <spelersnaam> <aantal dagen> <reden>");

				return true;

			}

			if (!Bukkit.getOfflinePlayer(args[0]).hasPlayedBefore()) {

				sender.sendMessage(prefix + ChatColor.RED + "De speler " + args[0] + " is niet gevonden.");

				return true;

			}

			int days = 0;

			try {

				days = Integer.parseInt(args[1]);

			} catch (Exception e) {

				sender.sendMessage(prefix + ChatColor.RED + args[1]
						+ " moet een getal zijn. Gelieve het aantal dagen op te geven.");
				return true;

			}

			String reason = this.getReason(args, 2);

			this.setBanned(args[0], days, reason);

			sender.sendMessage(prefix + ChatColor.GREEN + "De speler " + args[0] + " is succesvol voor " + days
					+ " dagen verbannen met als reden: \"" + reason + "\".");

			return true;

		}

		if (cmd.getName().equalsIgnoreCase("unban")) {

			if (args.length != 1) {

				sender.sendMessage(prefix + ChatColor.YELLOW + "/unban <spelersnaam>");

				return true;

			}

			OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);

			if (this.getConfig().getString("verbannen." + player.getUniqueId()) == null) {

				sender.sendMessage(prefix + ChatColor.RED + "De speler " + args[0] + " was niet verbannen.");

				return true;

			}

			this.getConfig().set("verbannen." + player.getUniqueId(), null);
			this.saveConfig();

			sender.sendMessage(
					prefix + ChatColor.GREEN + "De speler " + args[0] + " heeft succesvol een unban gekregen.");

			return true;

		}

		if (cmd.getName().equalsIgnoreCase("permban")) {

			if (args.length < 2) {

				sender.sendMessage(prefix + ChatColor.YELLOW + "/permban <spelersnaam> <reden>");

				return true;
			}

			if (!Bukkit.getOfflinePlayer(args[0]).hasPlayedBefore()) {

				sender.sendMessage(prefix + ChatColor.RED + "De speler " + args[0] + " is niet gevonden.");

				return true;

			}

			String reason = this.getReason(args, 1);

			this.setBanned(args[0], -1, reason);

			sender.sendMessage(prefix + ChatColor.GREEN + "De speler " + args[0]
					+ " is succesvol permanent verbannen met als reden: \"" + reason + "\".");

			return true;

		}

		if (cmd.getName().equalsIgnoreCase("kick")) {

			if (args.length < 2) {

				sender.sendMessage(prefix + ChatColor.YELLOW + "/kick <spelersnaam> <reden>");

				return true;

			}

			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {

				sender.sendMessage(prefix + ChatColor.RED + "De speler " + args[0] + " is niet gevonden.");

				return true;

			}

			String reason = this.getReason(args, 1);

			target.kickPlayer(ChatColor.RED + "U bent " + ChatColor.RED + "" + ChatColor.BOLD + "gekickt"
					+ ChatColor.RED + ".\n\n" + ChatColor.WHITE + "Reden: " + reason);

			sender.sendMessage(prefix + ChatColor.GREEN + "De speler " + args[0]
					+ " is succesvol gekickt met als reden: \"" + reason + "\".");

			return true;

		}

		if (cmd.getName().equalsIgnoreCase("mute")) {

			if (args.length < 3) {

				sender.sendMessage(prefix + ChatColor.YELLOW + "/mute <spelersnaam> <aantal dagen> <reden>");

				return true;

			}

			if (!Bukkit.getOfflinePlayer(args[0]).hasPlayedBefore()) {

				sender.sendMessage(prefix + ChatColor.RED + "De speler " + args[0] + " is niet gevonden.");

				return true;

			}

			int days = 0;

			try {

				days = Integer.parseInt(args[1]);

			} catch (Exception e) {

				sender.sendMessage(prefix + ChatColor.RED + args[1]
						+ " moet een getal zijn. Gelieve het aantal dagen op te geven.");
				return true;

			}

			String reason = this.getReason(args, 2);

			String unmuteTime = calculateTime(days);

			String uuid = "";

			if (Bukkit.getPlayer(args[0]) == null) {

				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);

				uuid = offlinePlayer.getUniqueId().toString();

			} else {

				Player player = Bukkit.getPlayer(args[0]);

				uuid = player.getUniqueId().toString();

				player.sendMessage(ChatColor.DARK_AQUA + "--------------------\n" + ChatColor.RED + "U bent "
						+ ChatColor.RED + "" + ChatColor.BOLD + "gemute.\n" + ChatColor.WHITE + "Reden: " + reason
						+ "\nTot: " + unmuteTime + ChatColor.DARK_AQUA + "\n--------------------");
			}

			this.getConfig().set("muted." + uuid + ".unmute", unmuteTime);
			this.getConfig().set("muted." + uuid + ".reden", reason);

			this.saveConfig();

			sender.sendMessage(prefix + ChatColor.GREEN + "De speler " + args[0] + " is succesvol voor " + days
					+ " dagen gemute met als reden: \"" + reason + "\".");

			return true;

		}

		if (cmd.getName().equalsIgnoreCase("unmute")) {

			if (args.length != 1) {

				sender.sendMessage(prefix + ChatColor.YELLOW + "/unmute <spelersnaam>");

				return true;

			}

			String uuid = "";

			if (Bukkit.getPlayer(args[0]) == null) {

				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);

				uuid = offlinePlayer.getUniqueId().toString();

			} else {

				Player player = Bukkit.getPlayer(args[0]);

				uuid = player.getUniqueId().toString();

			}

			this.getConfig().set("muted." + uuid, null);
			this.saveConfig();

			sender.sendMessage(prefix + ChatColor.GREEN + "De speler " + args[0] + " is succesvol ge-unmute.");

			return true;

		}

		return false;

	}

	private String calculateTime(int days) {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, days);

		return dateFormat.format(calendar.getTime());

	}

	private String getReason(String[] args, int begin) {

		String reason = "";

		for (int i = begin; i < args.length; i++) {

			reason += args[i] + " ";

		}

		reason = reason.trim();

		return reason;

	}

	@SuppressWarnings("deprecation")
	private void setBanned(String name, int days, String reason) {

		OfflinePlayer player = Bukkit.getOfflinePlayer(name);

		String unbanTime = "";

		if (days == -1) {

			if (player.isOnline()) {

				((Player) player).kickPlayer(ChatColor.DARK_RED + "" + ChatColor.BOLD + " PERMANENT VERBANNEN\n\n"
						+ ChatColor.WHITE + "Reden: " + reason);

			}

			unbanTime = "permanent";

		} else {

			unbanTime = calculateTime(days);

			if (player.isOnline()) {

				((Player) player).kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "TIJDELIJK VERBANNEN\n\n"
						+ ChatColor.WHITE + "Reden: " + reason + "\nTot: " + unbanTime);

			}

		}

		String uuid = player.getUniqueId().toString();

		this.getConfig().set("verbannen." + uuid + ".unban", unbanTime);
		this.getConfig().set("verbannen." + uuid + ".reden", reason);

		this.saveConfig();

	}

}
